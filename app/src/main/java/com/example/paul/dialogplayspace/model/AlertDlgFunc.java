package com.example.paul.dialogplayspace.model;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.paul.dialogplayspace.R;
import com.example.paul.dialogplayspace.activity.MainActivity;
import com.example.paul.dialogplayspace.util.BlurCustomUtil;
import com.example.paul.dialogplayspace.util.BlurDaliUtil;
import com.example.paul.dialogplayspace.util.RenderScriptGaussianBlur;

import at.favre.lib.dali.Dali;
import at.favre.lib.dali.builder.blur.BlurBuilder;

public class AlertDlgFunc {
    private final String TAG=getClass().getSimpleName();
    private final Context context;
    public static final int DIALOGFUNC_POSTIVE_CLICK=0;
    public static final int DIALOGFUNC_NEGATIVE_CLICK=1;
    public static final int DIALOG_POSITIVE_BUTTON=0;
    public static final int DIALOG_NEGATIVE_BUTTON=1;

    private View mDialogLyt;
    private LinearLayout mDialogBodyLyt;
    private TextView mTitleTv;
    private TextView mMessageTv;
    private Button mConfirmBtn;
    private Button mCancelBtn;
    private boolean outSideCancelable;
    private DialogFuncClick dialogFuncClick;

    private final String tag;

    private boolean cancelable=true;
    private AlertDialog alert;
    private boolean outsideCancelable=true;
//    private ImageView mBlurIv;
    private TextView mBlurMsgTv;
    private boolean isShowDialog = false;

    private RenderScriptGaussianBlur blur;

    public AlertDlgFunc(Context context, String tag) {
        this.context = context;
        this.tag = tag;
        init();
    }

    private void init() {
        mDialogLyt = LayoutInflater.from(context).inflate(R.layout.dialog_normal,null,false);
        mDialogBodyLyt = mDialogLyt.findViewById(R.id.dialog_ll_lyt);
        mTitleTv = mDialogLyt.findViewById(R.id.dialog_title_tv);
        mMessageTv = mDialogLyt.findViewById(R.id.dialog_message_tv);
        mConfirmBtn = mDialogLyt.findViewById(R.id.dialog_confirm_btn);
        mConfirmBtn.setText("ok");
        mCancelBtn = mDialogLyt.findViewById(R.id.dialog_cancel_btn);
        mCancelBtn.setText("cancel");
//        mBlurIv = mDialogLyt.findViewById(R.id.blur_bg_iv);
        mBlurMsgTv = mDialogLyt.findViewById(R.id.blur_message_tv);

    }

    public AlertDlgFunc setBlurBehind(){
        BlurDaliUtil blurDaliUtil = new BlurDaliUtil(context);
//        blurDaliUtil.simpleBlur1(R.drawable.test_img1,mBlurIv,mBlurMsgTv);
//        blurDaliUtil.simpleBlur2(R.drawable.test_img1,mBlurIv,mBlurMsgTv);
        ViewGroup lyt = ((Activity)context).findViewById(android.R.id.content);

        blurDaliUtil.simpleBlur5(mBlurMsgTv);

//        BlurCustomUtil blurCustomUtil = new BlurCustomUtil(context);
//        blurCustomUtil.blurStaticImage(R.drawable.test_img1,mBlurIv);
//        blurCustomUtil.blurActivtyTopView(mBlurIv);
        return this;
    }

    public AlertDlgFunc title(String titleString) {
        mTitleTv.setText(titleString);
        return this;
    }

    public AlertDlgFunc message(String messageString) {
        mMessageTv.setText(messageString);
        return this;
    }

    public AlertDlgFunc outSideCancelable(boolean outSideCancelable) {
        this.outSideCancelable = outSideCancelable;
        return this;
    }

    public AlertDlgFunc callBack(DialogFuncClick dialogFuncClick) {
        if(this.dialogFuncClick==null)this.dialogFuncClick = dialogFuncClick;
        return this;
    }

    public void show()
    {
//        addShadow(R.drawable.shadow_1876);
        if(mConfirmBtn.getVisibility()!=View.GONE)mConfirmBtn.setOnClickListener(clickListener);
        if(mCancelBtn.getVisibility()!=View.GONE)mCancelBtn.setOnClickListener(clickListener);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(cancelable);
        builder.setView(mDialogLyt);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(dialogFuncClick!=null)dialogFuncClick.onDismiss();
            }
        });
//        if(BaseApplication.isHighRes)
//        {
//            RelativeLayout.LayoutParams rl=(RelativeLayout.LayoutParams)dialogView.getLayoutParams();
//            rl.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            rl.leftMargin=(int)AndroidUtil.convertDpToPixel(45f,context);
//        }
        alert = builder.create();
        alert.setCanceledOnTouchOutside(outsideCancelable);
        try {alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }catch (Exception e){e.printStackTrace();}
//        if(BaseApplication.isHighRes)
//        {
//            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            WindowManager.LayoutParams wmlp = alert.getWindow().getAttributes();
//            wmlp.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
//            wmlp.x=0;
//            wmlp.y=0;
//        }
        try {
            alert.show();
            isShowDialog = true;
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public boolean isShowing() {
        return isShowDialog;
    }

    public void showDialog() {
        alert.show();
        isShowDialog = true;
    }

    public void hide() {
        alert.hide();
        isShowDialog = false;
    }

    public void dismiss() {
        alert.dismiss();
        isShowDialog = false;
    }


    public interface DialogFuncClick {
        void onClick(String tag, int action);
        void onDismiss();
    }

    private View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view==mConfirmBtn)
            {
                Log.d(TAG,"if(view==mConfirmBtn)");
//                if(alert!=null&&alert.isShowing() && !handleDissmiss)alert.dismiss();
                if(dialogFuncClick!=null)dialogFuncClick.onClick(tag,DIALOGFUNC_POSTIVE_CLICK);
                isShowDialog = false;
            }
            else if(view==mCancelBtn)
            {
                Log.d(TAG,"if(view==mCancelBtn)");
//                if(alert!=null&&alert.isShowing() && !handleDissmiss)alert.dismiss();
                if(dialogFuncClick!=null)dialogFuncClick.onClick(tag,DIALOGFUNC_NEGATIVE_CLICK);
                isShowDialog = false;
            }
        }
    };
}

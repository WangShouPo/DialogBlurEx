package com.example.paul.dialogplayspace.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.paul.dialogplayspace.R;

import at.favre.lib.dali.Dali;

public class BlurCustomUtil {

    private Context context;
    private Dali dali;
    private RenderScriptGaussianBlur blur;
    private String TAG = getClass().getSimpleName();

    public BlurCustomUtil(Context context) {
        this.context = context;
        dali = Dali.create(context);
    }

    public void blurStaticImage(int resourceId, ImageView targetImageView) {
        blur = new RenderScriptGaussianBlur(context);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
        targetImageView.setImageBitmap(blur.gaussianBlur(20, bitmap));

    }

    // TODO: how to config brightness
    public void blurActivtyTopView(ImageView targetImageView) {
        blur = new RenderScriptGaussianBlur(context);

        Bitmap b;
        View v = (ViewGroup) ((ViewGroup) ((Activity)context).findViewById(android.R.id.content)).getChildAt(0);
        Log.d(TAG,"v.getMeasuredHeight():"+v.getMeasuredHeight());
        Log.d(TAG,"v.getMeasuredWidth():"+v.getMeasuredWidth());
        Log.d(TAG,"v.getLayoutParams().width:"+v.getLayoutParams().width);
        Log.d(TAG,"v.getLayoutParams().height:"+v.getLayoutParams().height);
        if (v.getMeasuredHeight() <= 0) {
            v.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            v.draw(c);
        } else {
            b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            v.draw(c);
        }
        targetImageView.setImageBitmap(blur.gaussianBlur(20, b));

    }
}
package com.example.paul.dialogplayspace.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.paul.dialogplayspace.R;
import com.example.paul.dialogplayspace.model.AlertDlgFunc;

public class MainActivity extends AppCompatActivity {

    private AlertDlgFunc exitDialog;
    private boolean base_force_update=false;
    private Button mShowDialogBtn;
    private String TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mShowDialogBtn = findViewById(R.id.showDialog_btn);
        mShowDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExitDialog();
            }
        });
    }

    private void showExitDialog() {
        if(exitDialog==null)
        {
            exitDialog=new AlertDlgFunc(this,"exit");
            exitDialog.title("退出")
                    .message("是否退出?")
                    .outSideCancelable(!base_force_update)
                    .callBack(new AlertDlgFunc.DialogFuncClick() {
                        @Override
                        public void onClick(String tag, int action) {
                            Log.d(TAG, ".callBack Override onClick ");
                            if (tag.equals("exit") && action == AlertDlgFunc.DIALOGFUNC_POSTIVE_CLICK) {
                                Log.d(TAG, ".callBack Override onClick DIALOGFUNC_POSTIVE_CLICK");
                                exitApplication();
                            }
                            if (tag.equals("exit") && action == AlertDlgFunc.DIALOG_NEGATIVE_BUTTON) {
                                Log.d(TAG, ".callBack Override onClick DIALOG_NEGATIVE_BUTTON");
                                exitDialog.dismiss();
                            }
                        }

                        @Override
                        public void onDismiss() {
//                            activity_start=false;
//                            if(base_force_update)
//                            {
//                                showUpdateDialog(base_force_update,base_update_message);
//                            }
                            Log.d(TAG, ".callBack Override onDismiss ");
                            exitDialog.dismiss();
                        }
                    })
                    .setBlurBehind()
                    .show();
        }
        else {
            if(!exitDialog.isShowing()) exitDialog.showDialog();
        }
    }

    private void exitApplication()
    {
//        base_force_update=false;
//        if (getApplication() instanceof BaseApplication)
//        {
//            ((BaseApplication) getApplication()).onDestroy();
//        }
//        superFinish();
        finish();
    }
}
